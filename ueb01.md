# setting

You're given a git repo checkout which you know nothing about.

# task

Find out what the repo is about:
- what's the status of the repo
- what branches exist
- what remotes exist

# walkthrough

- notice that we are on branch `dev`
- notice that the working directory is not clean
- notice the python code does not execute
- check that the code would execute without the changes => `git stash`
- execute the python code `python3 code.py`
- now pop the changes => `git stash pop`
- fix the code
- check if the tests pass. **WHAT???** => `python3 code_test.py`
- fix the tests
- commit the code
- push

- notice the branches `git branch` and `git branch -a`
- notice that typing git is long => create a shell `alias git=g`
- inspect the branches
  - `g log dev`, `g log master`, `g log verbose`
  - `g show-branch master dev verbose`
  - `g diff master..dev`
  - `g diff dev..verbose`
  - `g diff master..verbose`
- notice that `g log` is verbose => create a git alias `git config --global alias.hist log --pretty=format:"%C(yellow)%h%Creset %ad %<(15)%Cgreen%ar%Creset %<(25)%an | %Cred%d%Creset %s" --graph --date=short`
- notice that `g diff` could be aliased to `g d` via `git config --global alias.d diff`

- notice the remotes `g remote -v`
- naming convention: origin for the own repo, upstream for upstream (maybe public) repo
- all are local remotes (without need for ssh)
