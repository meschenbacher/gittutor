#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

ueb01 () {
	git init --bare .ueb01.git
	git init --bare .ueb01.upstream.git
	git init ueb01 && cd ueb01
	git remote add origin ../.ueb01.git
	git remote add upstream ../.ueb01.upstream.git
	cat <<EOF > code.py
def main():
	print("hello world")

if __name__ == '__main__':
	main()
EOF
	cat <<EOF > code_test.py
import unittest
from unittest.mock import patch
from io import StringIO
from code import main

class TestMain(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    def test_main_output(self, stdout):
        main()
        self.assertEqual(stdout.getvalue(), "hello world\n")

if __name__ == '__main__':
    unittest.main()
EOF
	git add *.py
	git commit -m 'add code'
	git push -u origin master
	git checkout -b dev
	git push -u origin dev
	git checkout -b verbose
	cat <<EOF > code.py
import sys

def main():
	print("hello world")
	print("list of arguments", sys.argv)

if __name__ == '__main__':
	main()
EOF
	git commit -m "code: print arguments" code.py
	git push -u origin verbose
	git checkout dev
	cat <<EOF > code.py
def main():
	print("Hello, World!")

if __name__ == '__main__':
	main()
EOF
}

ueb01
